# Step 2 - Project structure
The application consists of a single GitLab "group", [balena-hello-world](https://gitlab.com/balena-hello-world).
The group includes three GitLab "projects" (git-repositories):
- [caddy](https://gitlab.com/balena-hello-world/caddy)
    - Webserver to act as a proxy between the world and application.
- [flask](https://gitlab.com/balena-hello-world/deployment)
    - A Python application serving the website content.
- [deployment](https://gitlab.com/balena-hello-world/deployment)
    - `docker-compose.yml` for balenaCloud deployment
    - `docker-compose.dev.yml` used in local development
    - Docker registry for hosting the Docker images built by the other repositories

So in essence:
```sh
mkdir balena-hello-world
cd balena-hello-world
git clone git@gitlab.com:balena-hello-world/deployment.git
git clone git@gitlab.com:balena-hello-world/caddy.git
git clone git@gitlab.com:balena-hello-world/flask.git
cd ..
tree -L 1
```

which should output something like:
```sh
.
├── caddy
│   ├── Caddyfile
│   ├── Dockerfile.amd64
│   ├── Dockerfile.raspberrypi3
│   └── README.md
├── deployment
│   ├── doc
│   ├── docker-compose.dev.yml
│   ├── docker-compose.yml
│   └── README.md
└── flask
    ├── app
    ├── Dockerfile.amd64
    ├── Dockerfile.raspberrypi3
    └── README.md
```
