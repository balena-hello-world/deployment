# Step 5 - Magically building containers
At this stage, both `caddy` and `flask` -repositories should have the correct
variables setup for the CI/CD to do it's work, and build the images for you.

With this setup, the images are built by the GitLab CI everytime something is
*pushed* to the `master` branch. The images are built for both `amd64` and `raspberrypi3`
and tagged `amd64-latest` and `amd64-<DATE>` (same for `raspberrypi3` of course).

Then, a manifest is created to allow accessing them via tags `latest` and `<DATE>`,
no mather what the platform and/or architecture is.
(note: unfortunately manifest support in balenaCloud isn't that good yet, but hoping it'll get better).

Both the images and manifest are pushed to the Docker registry, allowing easy
reuse of the images.

This is done by the `.gitlab-ci.yml` included in both of the repositories.
Both the repositories share the same `.gitlab-ci.yml`.
A "break-out" of the CI is presented below.

We're using two containers for building the images.
```yaml
# Run the build-process in a container with a stable build of Docker client
image: docker:stable

# Add a Docker-in-Docker container to actually build the images
services:
  - docker:dind

variables:
  # Define the Docker host for the client, using the service defined above
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  # Enable experimental features, allowing manifest creation
  DOCKER_CLI_EXPERIMENTAL: enabled
```

The CI consists of to stages, one for building and pushing the images and
one for creating the manifests, if previous stage succeeded.
```yaml
stages:
  - build_and_push
  - create_manifest
```

Before running any scripts, we want to setup the environment:
```yaml
before_script:
  # Create a DATE environment variable for tagging the images
  - export DATE=$(date +%Y-%m-%d)
  # Login to the Docker registry
  - docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY
```

Then we define the steps done in each stage:
```yaml
build_and_push:
  stage: build_and_push
  # This build should only be run on master -branch
  only:
    refs:
      - master
  script:
    # Pull the latest images to utilize Docker layer cache
    - docker pull $REGISTRY/$IMAGE:amd64-latest || true
    - docker pull $REGISTRY/$IMAGE:raspberrypi3-latest || true

    # Build new images, using the cache fetched in the previous step and tagging them appropriately
    - docker build --cache-from $REGISTRY/$IMAGE:amd64-latest --tag $REGISTRY/$IMAGE:amd64-latest --tag $REGISTRY/$IMAGE:amd64-$DATE -f Dockerfile.amd64 .
    - docker build --cache-from $REGISTRY/$IMAGE:raspberrypi3-latest --tag $REGISTRY/$IMAGE:raspberrypi3-latest --tag $REGISTRY/$IMAGE:raspberrypi3-$DATE -f Dockerfile.raspberrypi3 .

    # Push the fresh images to the registry
    - docker push $REGISTRY/$IMAGE:amd64-latest
    - docker push $REGISTRY/$IMAGE:amd64-$DATE
    - docker push $REGISTRY/$IMAGE:raspberrypi3-latest
    - docker push $REGISTRY/$IMAGE:raspberrypi3-$DATE
```

```yaml
create_manifest:
  stage: create_manifest
  only:
    refs:
      - master
  script:
    # Create a new manifest for "latest" -tag
    - docker manifest create $REGISTRY/$IMAGE:latest --amend $REGISTRY/$IMAGE:amd64-latest --amend $REGISTRY/$IMAGE:raspberrypi3-latest
    # Annotate the images appropriately, to make sure architectures match etc
    - docker manifest annotate $REGISTRY/$IMAGE:latest $REGISTRY/$IMAGE:amd64-latest --os linux --arch amd64
    - docker manifest annotate $REGISTRY/$IMAGE:latest $REGISTRY/$IMAGE:raspberrypi3-latest --os linux --arch arm --variant v7hf
    # Push the new manifest to registry
    - docker manifest push --purge $REGISTRY/$IMAGE:latest

    # Same steps for the date-based image
    - docker manifest create $REGISTRY/$IMAGE:$DATE --amend $REGISTRY/$IMAGE:amd64-$DATE --amend $REGISTRY/$IMAGE:raspberrypi3-$DATE
    - docker manifest annotate $REGISTRY/$IMAGE:$DATE $REGISTRY/$IMAGE:amd64-$DATE --os linux --arch amd64
    - docker manifest annotate $REGISTRY/$IMAGE:$DATE $REGISTRY/$IMAGE:raspberrypi3-$DATE --os linux --arch arm --variant v7hf
    - docker manifest push --purge $REGISTRY/$IMAGE:$DATE
```
