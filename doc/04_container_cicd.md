# Step 4 - Setting up CI/CD for containers
The next step is to setup CI/CD for both of the containers running in the
multicontainer application (`caddy` and `flask`).

To achieve this, envirinment variables need to be setup for all container
repositories in `Settings -> CI/CD -> Variables`. The GIF below should help you
find it...

![GIF 1](image/04/01.webm)

In here, you need to setup 4 variables:
- `IMAGE` -> name of the image to create (`caddy` or `flask` in this example, depending on the repository)
- `REGISTRY` -> path for the Docker registry to create the images to (`registry.gitlab.com/balena-hello-world/deployment` in this example)
- `REGISTRY_USER` -> your GitLab username
- `REGISTRY_PASSWORD` -> Value of Personal Access Token created in the previous step

Repeat the setup for both (all) the container repositories.

All the variables can be kept as `Protected`. By using `Protected`,
the variables are only exposed when pushing to protected repositories
(`master` by default), which prevents them leaking through pull requests etc.
This also means, that images are only built from protected branches.

`REGISTRY_PASSWORD` should always be masked. This way it's replaced by asterisks,
even if `.gitlab-ci.yml` is setup to expose it.
