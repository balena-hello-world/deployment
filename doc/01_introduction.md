# Step 1 - Setting up a GitLab CI and balenaCloud
This example project shows how to setup GitLab CI and balenaCloud working together.

The project is used to deploy a multicontainer application with each container
maintained in a separate repository. Each repository is also responsible for
building it's own Docker images and pushing them to the Docker registry hosted
by GitLab. The image building an pushing is done automatically by GitLab CI.

The project builds Docker images for multiple architectures
(`amd64` and `armv7hf` for Raspberry Pi 3).
The `amd64` is used for local development, while `raspberrypi3` images are
pushed to balenaCloud.

These pre-built images are then used via `docker-compose.yml` to deploy
the multicontainer application.
Also pushing to balenaCloud is automated via GitLab CI.
