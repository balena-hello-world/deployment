# Step 3 - Creating a personal access token
First step in setting up the build system is to create a personal access token,
used for accessing the repository while building the images etc.

To create a token, go to [User -> Settings -> Access Tokens](https://gitlab.com/profile/personal_access_tokens) in GitLab.
In there, create a new token with `api` scope defined, as shown in the pictures below (name doesn't mather). Keep this safe, as it allows full access to GitLab as **you**.

**Also, make sure to store the access token, as you won't be able to see it again!**

![Image 1](image/03/01.png)
![Image 2](image/03/02.png)
