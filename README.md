# balena-hello-world
This [balena-hello-world](https://gitlab.com/balena-hello-world) -group implements an exemplary CI/CD -workflow for a [balenaCloud](https://www.balena.io/cloud/) -project, utilizing multiple containers.
Each container is maintained in it's own project, pushing Docker images to the [deployment registry](https://gitlab.com/balena-hello-world/deployment/container_registry).
[deployment](https://gitlab.com/balena-hello-world/deployment) -project is then used to push the whole project to [balenaCloud](https://www.balena.io/cloud/).

## balena-hello-world/deployment
This repository contains the deployment -part
of [balena-hello-world](https://gitlab.com/balena-hello-world) -application.

[deployment](https://gitlab.com/balena-hello-world/deployment) -repository
takes care of maintaining the Docker Registry and deploying the project to
[balenaCloud](https://www.balena.io/cloud/).
